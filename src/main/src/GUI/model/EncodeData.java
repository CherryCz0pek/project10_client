package GUI.model;

import java.io.Serializable;

public class EncodeData implements Serializable {
    private String encodeMessage;



    public EncodeData() {
    }

    public EncodeData(String encodeMessage) {
        this.encodeMessage = encodeMessage;
    }

    public String getEncodeMessage() {
        return encodeMessage;
    }
}