package GUI.model;

import java.io.Serializable;

public class DecodeData implements Serializable {
    private String decodeMessage;



    public DecodeData() {
    }

    public DecodeData(String decodeMessage) {
        this.decodeMessage = decodeMessage;
    }

    public String getDecodeMessage() {
        return decodeMessage;
    }
}