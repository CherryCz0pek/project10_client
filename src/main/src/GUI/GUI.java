package GUI;

import GUI.model.DecodeData;
import GUI.model.EncodeData;
import GUI.model.ReturnData;
import com.google.gson.Gson;


import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;

import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;

import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;


import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;

public class GUI extends JFrame implements ActionListener {

    private JButton bEncode, bDecode, bClean;
    private JLabel poczOpis, koncOpis;
    private JTextArea pSzyfr,kSzyfr;
    public GUI() {

        setSize(300, 300);
        setLocationByPlatform(true);
        setLayout(null);
        poczOpis = new JLabel("Wprowadź tekst do szyfrowania/deszyfrowania:");
        poczOpis.setBounds(10, 5, 280, 30);
        add(poczOpis);

        pSzyfr = new JTextArea(3,20);
        pSzyfr.setLineWrap(true);
        pSzyfr.setBounds(10, 40, 280, 50);
        add(pSzyfr);


        bEncode = new JButton("Szyfruj");
        add(bEncode);
        bEncode.setBounds(10, 100, 90, 70);
        bEncode.addActionListener(this);

        bDecode = new JButton("Deszyfruj");
        add(bDecode);
        bDecode.setBounds(105, 100, 90, 70);
        bDecode.addActionListener(this);

        bClean = new JButton("Czyść");
        add(bClean);
        bClean.setBounds(200, 100, 90, 70);
        bClean.addActionListener(this);


        koncOpis = new JLabel("Tekst zaszyfrowany / odszyfrowany:",SwingConstants.CENTER);
        koncOpis.setBounds(10, 180, 280, 30);
        add(koncOpis);

        kSzyfr = new JTextArea(3,40);
        kSzyfr.setBounds(10, 215, 280, 50);
        kSzyfr.setEditable(false);
        add(kSzyfr);
    }

    public void actionPerformed(ActionEvent e) {
        String textPocz = pSzyfr.getText();
        String textKonc;
        Object source = e.getSource();
        if(source == bEncode)
        {

          textKonc=httpClientForEncodeMethod(textPocz);
          kSzyfr.setText(textKonc);

        }
        else if (source == bDecode)
        {
            textKonc=httpClientForDecodeMethod(textPocz);
            kSzyfr.setText(textKonc);
        }
        else{
            pSzyfr.setText("");
            kSzyfr.setText("");
        }
    }

    private static String httpClientForEncodeMethod(String str) {

        final CloseableHttpClient client = HttpClients.createDefault();
        final HttpPost httpPost = new HttpPost("http://127.0.0.1:8080/api/fence/encode");

        Gson gson = new Gson();

        final EncodeData encodeData = new EncodeData(str);
        final String json = gson.toJson(encodeData);

        try {

            final StringEntity entity = new StringEntity(json);
            httpPost.setEntity(entity);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            final CloseableHttpResponse response = client.execute(httpPost);

            System.out.println("Kod odpowiedzi serwera: " + response.getStatusLine().getStatusCode());

            if(response.getStatusLine().getStatusCode() == 404) {

                System.out.println("Prosze poprawic w kontrolerze sciezke do pliku - sciezka jest nieprawidlowa!");
            } else if(response.getStatusLine().getStatusCode() == 200) {

                final HttpEntity httpEntity = response.getEntity();

                // Na tym etapie odczytujemy JSON'a, ale jako String.
                final String jSonAsReturnData = EntityUtils.toString(httpEntity);

                final ReturnData returnData = gson.fromJson(jSonAsReturnData, ReturnData.class);
                // Dzialamy na obiekcie - mamy dostep do danych, ktore zostaly odczytane z JSON'a
                str = returnData.getResult();


            }

            client.close();

        } catch (ConnectException e) {

            System.out.println("Nie moge polaczyc sie z serwerem");
            System.out.println();

        } catch (UnsupportedEncodingException e) {

            System.out.println("Houston, we have a problem with POST unsupported encoding");
            e.printStackTrace();
        } catch (ClientProtocolException e) {

            System.out.println("Houston, we have a problem with POST client protocol");
            e.printStackTrace();
        } catch (IOException e) {

            System.out.println("Houston, we have a problem with POST input output");
            e.printStackTrace();
        }
        return str;
    }

    private static String httpClientForDecodeMethod(String str) {

        final CloseableHttpClient client = HttpClients.createDefault();
        final HttpPost httpPost = new HttpPost("http://127.0.0.1:8080/api/fence/decode");

        Gson gson = new Gson();

        final DecodeData decodeData = new DecodeData(str);
        final String json = gson.toJson(decodeData);

        try {

            final StringEntity entity = new StringEntity(json);
            httpPost.setEntity(entity);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            final CloseableHttpResponse response = client.execute(httpPost);

            System.out.println("Kod odpowiedzi serwera: " + response.getStatusLine().getStatusCode());

            if(response.getStatusLine().getStatusCode() == 404) {

                System.out.println("Prosze poprawic w kontrolerze sciezke do pliku - sciezka jest nieprawidlowa!");
            } else if(response.getStatusLine().getStatusCode() == 200) {

                final HttpEntity httpEntity = response.getEntity();

                // Na tym etapie odczytujemy JSON'a, ale jako String.
                final String jSonAsReturnData = EntityUtils.toString(httpEntity);

                final ReturnData returnData = gson.fromJson(jSonAsReturnData, ReturnData.class);
                // Dzialamy na obiekcie - mamy dostep do danych, ktore zostaly odczytane z JSON'a
                str = returnData.getResult();

            }

            client.close();

        } catch (ConnectException e) {

            System.out.println("Nie moge polaczyc sie z serwerem");
            System.out.println();

        } catch (UnsupportedEncodingException e) {

            System.out.println("Houston, we have a problem with POST unsupported encoding");
            e.printStackTrace();
        } catch (ClientProtocolException e) {

            System.out.println("Houston, we have a problem with POST client protocol");
            e.printStackTrace();
        } catch (IOException e) {

            System.out.println("Houston, we have a problem with POST input output");
            e.printStackTrace();
        }
        return str;
    }


}